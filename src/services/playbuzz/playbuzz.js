export default function getPlayBuzzContent () {
  return fetch('http://rest-api-v2.playbuzz.com/v2/items?sort=viewCount&size=25')
    .then(function (response) {
      const contentType = response.headers.get("content-type");
      if(contentType && contentType.includes("application/json")) {
        return response.json();
      }
      throw new TypeError('Invalid content type.');
    })
    .then( (json = {}) => {
      const { code, payload } = json;
      if (code === 200 && payload && payload.items && payload.items.length > 0) {
        return payload.items;
      }
      throw new TypeError('Invalid payload or code.');
    });
}