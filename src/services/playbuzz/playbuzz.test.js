import getPlayBuzzContent from './playbuzz'

describe('playbuzz service', () => {
  global.fetch = jest.fn();

  function buildMockedReturnedValue (response) {
    return Promise.resolve({
      headers: {
        get: () => {
          return {
            includes: () => true
          }
        }
      },
      json: () => Promise.resolve(response)
    })
  }

  function mockResponse (response) {
    const mockedReturnedValue = buildMockedReturnedValue(response);
    global.fetch.mockReturnValueOnce(mockedReturnedValue);
  }

  it('errors when status code is not 200', () => {
    mockResponse({
      code: 404,
      payload: {
        items: [{ object: 1 }, { object: 2 }]
      }
    });

    expect.assertions(1);
    return getPlayBuzzContent()
      .catch((err) => {
        expect(err).toEqual(new TypeError('Invalid payload or code.'));
      });
  });

  it('errors when payload has an empty items list', () => {
    mockResponse({
      code: 200,
      payload: {
        items: []
      }
    });

    expect.assertions(1);
    return getPlayBuzzContent()
      .catch((err) => {
        expect(err).toEqual(new TypeError('Invalid payload or code.'));
      });
  });

  it('resolves when payload has items in its list and status code is 200', () => {
    mockResponse({
      code: 200,
      payload: {
        items: [{ object: 1 }, { object: 2 }]
      }
    });

    expect.assertions(1);
    return getPlayBuzzContent()
      .then((res) => {
        expect(res).toEqual([{ object: 1 }, { object: 2 }]);
      })
  });
});
