import React, { Component } from 'react';
import './app.css';
// import power from './assets/power.svg';
import Visual from '../visual/visual';
import Tab from '../tab/tab';
import Error from '../error/error';
import About from '../about/about';
import getPlayBuzzContent from '../../services/playbuzz/playbuzz';

class App extends Component {
  constructor(props) {
    super(props);
    const tabs = [
      // {
      //   name: 'reset',
      //   onclick: () => {
      //     this.setState({
      //       view: <Tab></Tab>
      //     });
      //   }
      // },
      {
        name: 'start',
        onclick: () => {
          getPlayBuzzContent()
            .then((res) => {
              this.setState({
                view: <Visual src={res}/>
              });
            })
            .catch((err) => {
              this.setState({
                view: <Error src={err}/>
              });
            });

        }
      },
      {
        name: 'about',
        onclick: () => {
          this.setState({
            view: <About/>
          });
        }
      }
    ];

    this.state = {
      tabs: tabs,
      view: <About/>
      // view: <img src={power} alt=""/>
    };
  }

  render() {
    return (
      <div className="app">
        <div className="app__header">
          <h2>Welcome to Uni Visual</h2>
        </div>
        <div className="app__body">
          {
            this.state.tabs.map(
              (tab, index) =>
                <Tab key={index} name={tab.name} onclick={tab.onclick}></Tab>
            )
          }
          {this.state.view}
        </div>
      </div>
    );
  }
}

export default App;
