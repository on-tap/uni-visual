import React, { Component } from 'react';

class About extends Component {
  render () {
    return (
      <div>
        <h1>About</h1>
        <p>This is a simple app that will display the most trending Playbuzz items.</p>
      </div>
    );
  }
}

export default About;