import React, { Component } from 'react';
import './tab.css';

class Tab extends Component {
  render () {
    return (
      <div className="tab" onClick={this.props.onclick}>{this.props.name}</div>
    );
  }
}

export default Tab;