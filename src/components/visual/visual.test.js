import React from 'react';
import ReactDOM from 'react-dom';
import Visual from './visual';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const mockList = [
    {
      viewCount: 100
    }
  ];
  ReactDOM.render(<Visual src={mockList} />, div);
});
