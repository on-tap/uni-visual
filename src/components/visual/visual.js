import React, { Component } from 'react';
import * as d3 from "d3";
import './visual.css';

class Visual extends Component {
  constructor(props) {
    super(props);
    const max = props.src[0];
    const min = props.src[props.src.length - 1];
    this.state = {
      max: max && max.viewCount,
      min: min && min.viewCount
    };
  }

  componentDidMount () {
    const delta = this.state.max - this.state.min;
    d3.select('.visual')
      .selectAll('div')
      .data(this.props.src)
      .enter().append('div')
      .style('background-image', (d) => {
        const intensity = (d.viewCount - this.state.min)/delta;
        const centerRgba = d3.interpolateRgb("#b9c0bf", "#263238")(intensity);
        return `radial-gradient(ellipse at center, ${centerRgba} 25%, #ECEFF1 65%)`;
      })
      .insert((d) => {
        const div = document.createElement('div');
        div.className = 'visual-data';
        div.style.backgroundImage = `url(${d.imageMedium})`;
        return div;
      });
  }

  render () {
    return (
      <div className="visual">
      </div>
    );
  }
}

export default Visual;