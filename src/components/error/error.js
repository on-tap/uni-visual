import React, { Component } from 'react';

class Error extends Component {
  constructor(props) {
    super(props);
    console.error(props.src);
  }
  render() {
    return (
      <div className="error">
        <div className="error__title">
          <h2>Oh no! Something went wrong! :(</h2>
        </div>
      </div>
    );
  }
}

export default Error;