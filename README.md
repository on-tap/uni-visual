# Uni-Visual

This app uses [d3](https://d3js.org/) to display [Playbuzz's](http://www.playbuzz.com/) most popular content items. The item's popularity is determine by the view count associated with that item. The highest view count will result with an item being displayed with a darker elliptical background.  

## Getting Started

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

## Built With

* [Create React App](https://github.com/facebookincubator/create-react-app) - Creats React apps with no build configuration.

